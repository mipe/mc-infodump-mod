package org.sysvyz.infodump

import cpw.mods.fml.common.Mod
import cpw.mods.fml.common.event.FMLInitializationEvent
import cpw.mods.fml.common.Mod.EventHandler
import net.minecraft.item.crafting.CraftingManager
import org.sysvyz.infodump.serializer.xml.XmlSerializer

@Mod(modid = "InfoDumpMod", name = "Info Dump Mod", version = "0.0.0", modLanguage = "scala")
object InfoDumpMod {

  @EventHandler
  def init(e: FMLInitializationEvent) {
    val cm = CraftingManager.getInstance
    val recipes = cm.getRecipeList()
    new XmlSerializer().serialize(recipes)
  }
}
package org.sysvyz.infodump.serializer.xml

import net.minecraft.item.crafting.ShapedRecipes
import scala.xml.Elem
import net.minecraft.item.ItemStack
import scala.collection.mutable.ListBuffer

trait ShapedRecipesTrait extends ShapedRecipes with XmlSerializable {

  def serialize: Elem = {
    val unlocalizedName = <UnlocalizedName>{ this.getRecipeOutput().getUnlocalizedName() }</UnlocalizedName>
    val displayName = <DisplayName>{ this.getRecipeOutput().getDisplayName() }</DisplayName>
    val recipeSize = <RecipeSize>{ this.getRecipeSize() }</RecipeSize>
    var items = new ListBuffer[Elem]
    for ((item, index) <- this.recipeItems.view.zipWithIndex) {
      if (item.isInstanceOf[ItemStack]) {
        val itemStack = item.asInstanceOf[ItemStack]
        val amount = <StackSize>{ itemStack.stackSize }</StackSize>
        val position = <Position>{ index }</Position>
        val itemUnlocalized = <UnlocalizedName>{ itemStack.getItem().getUnlocalizedName() }</UnlocalizedName>
        items += <Item>{ itemUnlocalized }{ amount }{ position }</Item>
      }
    }
    <Shaped>{ unlocalizedName }{ displayName }{ recipeSize }<Items>{ items }</Items></Shaped>
  }
}
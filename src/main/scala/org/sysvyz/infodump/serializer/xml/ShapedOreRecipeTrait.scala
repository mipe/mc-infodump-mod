package org.sysvyz.infodump.serializer.xml

import net.minecraftforge.oredict.ShapedOreRecipe
import scala.xml.Elem
import scala.collection.mutable.ListBuffer
import net.minecraft.item.ItemStack

trait ShapedOreRecipeTrait extends ShapedOreRecipe with XmlSerializable {

  def serialize: Elem = {
    val unlocalizedName = <UnlocalizedName>{ this.getRecipeOutput().getUnlocalizedName() }</UnlocalizedName>
    val displayName = <DisplayName>{ this.getRecipeOutput().getDisplayName() }</DisplayName>
    val recipeSize = <RecipeSize>{ this.getRecipeSize() }</RecipeSize>
    var items = new ListBuffer[Elem]
    for ((item, index) <- this.getInput().view.zipWithIndex) {
      if (item.isInstanceOf[ItemStack]) {
        val itemStack = item.asInstanceOf[ItemStack]
        val amount = <StackSize>{ itemStack.stackSize }</StackSize>
        val position = <Position>{ index }</Position>
        val itemUnlocalized = <UnlocalizedName>{ itemStack.getItem().getUnlocalizedName() }</UnlocalizedName>
        items += <Item>{ itemUnlocalized }{ amount }{ position }</Item>
      }
    }
    <ShapedOre>{ unlocalizedName }{ displayName }{ recipeSize }<Items>{ items }</Items></ShapedOre>
  }
}
package org.sysvyz.infodump.serializer.xml

import scala.xml.Elem
import org.sysvyz.infodump.serializer.Recipe

trait RecipeTrait extends Recipe with XmlSerializable {

  def serialize: Elem = {
    <Recipe/>
  }
}
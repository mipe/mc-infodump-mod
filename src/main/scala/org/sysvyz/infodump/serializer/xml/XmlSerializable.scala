package org.sysvyz.infodump.serializer.xml

import scala.xml.Elem

trait XmlSerializable {
  def serialize(): Elem
}
package org.sysvyz.infodump.serializer.xml
import scala.xml.Elem
import scala.collection.mutable.ListBuffer
import net.minecraft.item.ItemStack
import net.minecraftforge.oredict.ShapelessOreRecipe

trait ShapelessOreRecipeTrait extends ShapelessOreRecipe with XmlSerializable {

  def serialize: Elem = {
    val unlocalizedName = <UnlocalizedName>{ this.getRecipeOutput().getUnlocalizedName() }</UnlocalizedName>
    val displayName = <DisplayName>{ this.getRecipeOutput().getDisplayName() }</DisplayName>
    val recipeSize = <RecipeSize>{ this.getRecipeSize() }</RecipeSize>
    var items = new ListBuffer[Elem]
    for (item <- this.getInput().toArray()) {
      if (item.isInstanceOf[ItemStack]) {
        val itemStack = item.asInstanceOf[ItemStack]
        val amount = <StackSize>{ itemStack.stackSize }</StackSize>
        val itemUnlocalized = <UnlocalizedName>{ itemStack.getItem().getUnlocalizedName() }</UnlocalizedName>
        items += <Item>{ itemUnlocalized }{ amount }</Item>
      }
    }
    <ShapelessOre>{ unlocalizedName }{ displayName }{ recipeSize }<Items>{ items }</Items></ShapelessOre>
  }
}
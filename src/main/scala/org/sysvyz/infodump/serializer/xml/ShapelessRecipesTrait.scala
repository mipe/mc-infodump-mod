package org.sysvyz.infodump.serializer.xml
import net.minecraft.item.crafting.ShapelessRecipes
import scala.xml.Elem
import scala.collection.mutable.ListBuffer
import net.minecraft.item.ItemStack

trait ShapelessRecipesTrait extends ShapelessRecipes with XmlSerializable {

  def serialize: Elem = {
    val unlocalizedName = <UnlocalizedName>{ this.getRecipeOutput().getUnlocalizedName() }</UnlocalizedName>
    val displayName = <DisplayName>{ this.getRecipeOutput().getDisplayName() }</DisplayName>
    val recipeSize = <RecipeSize>{ this.getRecipeSize() }</RecipeSize>
    var items = new ListBuffer[Elem]
    for (item <- this.recipeItems.toArray()) {
      if (item.isInstanceOf[ItemStack]) {
        val itemStack = item.asInstanceOf[ItemStack]
        val amount = <StackSize>{ itemStack.stackSize }</StackSize>
        val itemUnlocalized = <UnlocalizedName>{ itemStack.getItem().getUnlocalizedName() }</UnlocalizedName>
        items += <Item>{ itemUnlocalized }{ amount }</Item>
      }
    }
    <Shapeless>{ unlocalizedName }{ displayName }{ recipeSize }<Items>{ items }</Items></Shapeless>
  }
}
package org.sysvyz.infodump.serializer.xml

import org.apache.commons.io.FileUtils
import java.io.File
import scala.xml.PrettyPrinter
import scala.collection.mutable.ListBuffer
import scala.xml.Elem
import net.minecraftforge.oredict.ShapedOreRecipe
import net.minecraft.item.crafting.ShapedRecipes
import net.minecraftforge.oredict.ShapelessOreRecipe
import net.minecraft.item.crafting.IRecipe
import net.minecraft.item.crafting.ShapelessRecipes
import net.minecraft.item.ItemStack
import scala.collection.mutable.HashMap
import net.minecraftforge.oredict.OreDictionary
import java.util.ArrayList
import cpw.mods.fml.common.ObfuscationReflectionHelper
import org.sysvyz.infodump.serializer.Recipe

class XmlSerializer {

  def extractShapedOre(recipe: ShapedOreRecipe): Array[Object] = {
    var list = new ListBuffer[Object]
    list += ObfuscationReflectionHelper.getPrivateValue(classOf[ShapedOreRecipe], recipe, "mirrored")
    for (elem <- recipe.getInput) {
      if (elem != null && elem.getClass.getSimpleName.equals("ArrayList")) {
        list += elem.asInstanceOf[ArrayList[Object]].get(0)
      }
    }
    list.toArray
  }

  def extractShapelessOre(list: Array[Object]): Array[Object] = {
    var listBuffer = new ListBuffer[Object]
    for (elem <- list) {
      if (elem != null && elem.getClass.getSimpleName.equals("ArrayList")) {
        listBuffer += elem.asInstanceOf[ArrayList[Object]].get(0)
      }
    }
    listBuffer.toArray
  }

  def recipeObjectPimper(recipe: Object): XmlSerializable = recipe match {
    case r: ShapedRecipes => new ShapedRecipes(r.recipeWidth, r.recipeHeight, r.recipeItems, r.getRecipeOutput) with ShapedRecipesTrait
    case r: ShapelessRecipes => new ShapelessRecipes(r.getRecipeOutput, r.recipeItems) with ShapelessRecipesTrait
    //    case r: ShapedOreRecipe => new ShapedOreRecipe(r.getRecipeOutput, extractShapedOre(r): _*) with ShapedOreRecipeTrait
    case r: ShapelessOreRecipe => new ShapelessOreRecipe(r.getRecipeOutput, extractShapelessOre(r.getInput.toArray): _*) with ShapelessOreRecipeTrait
    case r: IRecipe => new Recipe with RecipeTrait // any other unmatched implementations of IRecipe
    case _ => throw new ClassCastException
  }

  def serialize(list: java.util.List[_]) {
    var recipes = new ListBuffer[Elem]
    list.toArray.foreach(recipe => recipes += recipeObjectPimper(recipe).serialize)
    val root = <Recipes>{ recipes }</Recipes>
    val printer = new PrettyPrinter(100, 2)
    FileUtils.writeStringToFile(new File("/tmp/recipes.xml"), printer.format(root))
  }
}
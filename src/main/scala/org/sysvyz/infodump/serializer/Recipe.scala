package org.sysvyz.infodump.serializer

import net.minecraft.item.crafting.IRecipe
import net.minecraft.item.ItemStack
import net.minecraft.world.World
import net.minecraft.inventory.InventoryCrafting

class Recipe extends IRecipe {

  def getCraftingResult(var1: InventoryCrafting): ItemStack = null

  def matches(var1: InventoryCrafting, var2: World): Boolean = false

  def getRecipeSize(): Int = 0

  def getRecipeOutput(): ItemStack = null
}